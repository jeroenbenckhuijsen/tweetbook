package nl.group9.domain.user;

import nl.group9.domain.tweet.Tweet;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class User {

    private static long nextId = 1;

    private final long id;
    private final String name;
    private final Map<Long, Tweet> tweets;

    private long nextTweetId;

    public User(final String name) {
        this.id = getNextTweetId();
        this.tweets = new HashMap<>();
        this.name = name;
        nextTweetId = 1;
    }

    public Tweet addTweet(String message) {
        long nextTweetId = getNextTweetId();
        final Tweet tweet = new Tweet(nextTweetId, message, this);
        tweets.put(nextTweetId, tweet);
        return tweet;
    }

    public boolean removeTweet(final long id) {
        return tweets.remove(id) != null;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Map<Long, Tweet> getTweets() {
        return Collections.unmodifiableMap(tweets);
    }

    private long getNextTweetId() {
        return nextTweetId++;
    }

    private synchronized static long getNextId() {
        return nextId++;
    }
}
