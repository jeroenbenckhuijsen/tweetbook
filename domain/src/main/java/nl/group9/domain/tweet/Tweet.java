package nl.group9.domain.tweet;

import nl.group9.domain.user.User;

public class Tweet {

    private final Long id;
    private final String message;
    private final User user;

    public Tweet(final Long id, final String message, final User user) {
        this.id = id;
        this.message = message;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public User getUser() {
        return user;
    }
}
