package nl.group9.services;

import nl.group9.repository.api.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import nl.group9.domain.user.User;

@RestController()
public class TweetService {

    private UserRepository userRepository;

    @Autowired
    public TweetService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/nl/group9/domain/tweet")
    public void postMessage(@RequestParam(name = "id", required = true) final Long id, @RequestParam(name = "message", required = true) final String message) {
        User user = this.userRepository.get(id);
        user.addTweet(message);
        this.userRepository.save(user);
    }
}
