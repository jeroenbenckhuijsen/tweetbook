package nl.group9.services;

import nl.group9.repository.api.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import nl.group9.domain.user.User;

@RestController
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(method = RequestMethod.POST)
    public User register(@RequestParam(required = true, value = "name") final String name) {
        User newUser = new User(name);
        return this.userRepository.save(newUser);
    }


}
