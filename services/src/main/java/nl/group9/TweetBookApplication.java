package nl.group9;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("nl.group9")
public class TweetBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(TweetBookApplication.class, args);
    }
}
