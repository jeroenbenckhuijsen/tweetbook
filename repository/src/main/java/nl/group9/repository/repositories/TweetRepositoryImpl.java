package nl.group9.repository.repositories;

import nl.group9.domain.tweet.Tweet;
import nl.group9.repository.api.TweetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;

@Repository
public class TweetRepositoryImpl implements TweetRepository {

    private final HashMap<String, Tweet> tweetDatabase;

    @Autowired
    public TweetRepositoryImpl() {
        this.tweetDatabase = new HashMap<>();
    }

    public Tweet save(final Tweet tweet) {
        String id = tweet.getUser().getId() + "T" + tweet.getId();
        return tweetDatabase.put(id, tweet);
    }

    public Tweet get(final String id) {
        return tweetDatabase.get(id);
    }

    public Collection<Tweet> getAll() {
        return tweetDatabase.values();
    }
}
