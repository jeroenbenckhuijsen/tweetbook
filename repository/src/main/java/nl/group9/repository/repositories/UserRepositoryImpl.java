package nl.group9.repository.repositories;

import nl.group9.repository.api.UserRepository;
import nl.group9.domain.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final Map<Long, User> userDatabase;

    @Autowired
    public UserRepositoryImpl() {
        this.userDatabase = new HashMap<Long, User>();
    }


    @Override
    public User save(User user) {
        return userDatabase.put(user.getId(), user);
    }

    @Override
    public User get(Long id) {
        return userDatabase.get(id);
    }

    @Override
    public Collection<User> getAll() {
        return userDatabase.values();
    }
}
