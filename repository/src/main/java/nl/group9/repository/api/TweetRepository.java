package nl.group9.repository.api;

import nl.group9.domain.tweet.Tweet;

import java.util.Collection;

/**
 * Repository for {@link Tweet}s
 */
public interface TweetRepository {

    /**
     * Persist or update a {@link Tweet}
     *
     * @param tweet The {@link Tweet} to save.
     * @return the persisted {@link Tweet}
     */
    Tweet save(Tweet tweet);

    /**
     * Retrieves a {@link Tweet} with the provided id.
     *
     * @param id the id of the {@link Tweet}
     * @return a {@link Tweet} or null if not found
     */
    Tweet get(String id);

    /**
     * Returns all tweets
     *
     * @return all tweets.
     */
    Collection<Tweet> getAll();
}
