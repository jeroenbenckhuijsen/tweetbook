package nl.group9.repository.api;

import nl.group9.domain.user.User;

import java.util.Collection;

/**
 * Repository voor {@link User}s.
 */
public interface UserRepository {

    /**
     * Persists or updates the {@link User}
     *
     * @param user the {@link User} to persist.
     * @return The persisted {@link User}
     */
    User save(User user);

    /**
     * Retrieves a {@link User} with the provided id.
     *
     * @param id the id of the {@link User}
     * @return the {@link User}. Or <code>null</code> if not found.
     */
    User get(Long id);

    /**
     * Retrieves all {@link User}s
     *
     * @return them all
     */
    Collection<User> getAll();

}
